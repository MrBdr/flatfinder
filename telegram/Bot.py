__author__ = 'Admin'

import httplib
from urllib import quote, urlencode
import json
from time import sleep
from Message import Message
from Chat import Chat

class Bot:

    _api_url = "https://core.telegram.org/bots/api"
    _domain = "api.telegram.org"
    _bot_link = "telegram.me"

    def __init__(self, token, name, username):
        self.token = token
        self.name = name
        self.username = username

    def baseURL(self):
       return "/bot" + self.token

    def linkURL(self):
        return Bot._bot_link + "/" + self.username

    def sendMessage(self, chat, msg, original_msg_id = None, disable_preview = True):

        url = self.baseURL() + "/sendMessage"
        params = {
            "chat_id": chat.chat_id,
            "text": msg.text,
            "disable_web_page_preview": disable_preview
        }

        if original_msg_id is not None:
            params["reply_to_message_id"] = original_msg_id


        print params
        print url

        resp = Bot.TransactWithServer(url, "GET", params)
        print resp
        return resp[u"result"][u"message_id"]

    def sendLocation(self, chat, lat, lng, original_msg_id = None):

        url = self.baseURL() + "/sendLocation"
        params = {
            "chat_id": chat.chat_id,
            "latitude": lat,
            "longitude" : lng
        }

        if original_msg_id is not None:
            params["reply_to_message_id"] = original_msg_id

        print params
        print url

        return Bot.TransactWithServer(url, "GET", params)




    @staticmethod
    def TransactWithServer(url, method="GET", params={}, headers={}, data = None):

        sleep(3) # so wont overload the server?

        conn = httplib.HTTPSConnection( Bot._domain)
        if params:
            url += "?"+ urlencode(params)

        conn.request(method, url, data, headers)
        response = conn.getresponse()

        data = response.read()

        return json.loads(data) if data else None

    def getUpdates(self):

        url = self.baseURL() + "/getUpdates"

        return  Bot.TransactWithServer(url)





