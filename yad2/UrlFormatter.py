__author__ = 'Admin'
from time import time
from urllib import quote, urlencode

class UrlFormatter:
    _protocol = "http://"
    _domain = "www.yad2.co.il"


    _info_realtor_url = "/Nadlan/tivrent_info.php?NadlanID=%s"
    _info_url = "/Nadlan/rent_info.php?NadlanID=%s"
    _apts_url = "/ajax/Nadlan/searchMap/results.php?"

    def __init__(self):
        pass

    @staticmethod
    def FormateCoordinatePoint(name, point):
        return "coords[{0}][lat]={1}&coords[{0}][lng]={2}".format(name, point.lat, point.lng)

    @staticmethod
    def FormateCenterPoint(point):
        return "centerCoords[lat]={0}&centerCoords[lng]={1}".format(point.lat, point.lng)

    @staticmethod
    def FormatFilter(filter):
        # advacned filter:
        # http://www.yad2.co.il/Nadlan/rentMap.php?AreaID=73&City=&HomeTypeID=&fromRooms=2.5&untilRooms=4&fromPrice=&untilPrice=&PriceType=1&FromFloor=&ToFloor=&fromSquareMeter=&untilSquareMeter=&EnterDate=&Info=
        no_filter = "SubCatID=2&AreaID=73&City=&HomeTypeID=&fromRooms=&untilRooms=&fromPrice=&untilPrice=&PriceType=1&FromFloor=&ToFloor=&EnterDate=&Info="

        defaults = {
                    "SubCatID": "2", # must be 2, otherwise doesnt work
                    "AreaID" : "73",
                    "City" : "",
                    "HomeTypeID" : "",
                    "fromRooms" : "",
                    "untilRooms" : "",
                    "fromPrice" : "",
                    "untilPrice" : "",
                    "PriceType" : "1", # 1 for shekels?
                    "FromFloor" : "",
                    "ToFloor" : "",
                    "EnterDate" : "",
                    "Info" : "",
                    }
        filter_parts = {}
        filter_parts.update(defaults)

        if filter :
            for k in filter:
                if k not in defaults:
                    filter.pop(k)

            filter_parts.update(filter)


        filter_url_part = quote(urlencode(filter_parts), safe="/=&")
        # print filter_url_part
        return filter_url_part


    @staticmethod
    def GetInfoURL(app_id, is_realtor):
        """
        :param app_id:
        :return url for raw page:
        """
        if is_realtor:
           return UrlFormatter._info_realtor_url % app_id

        return UrlFormatter._info_url % app_id

    @staticmethod
    def GetApartmentsURL(radial_location, filter):
        """
        :param app_id:
        :return complex parsed object:
        """
        url = ""
        url_params = []
        timestamp = int(time())

        url_params.append(UrlFormatter.FormatFilter(filter))
        url_params.append(UrlFormatter.FormateCoordinatePoint("top",radial_location.top_right))
        url_params.append(UrlFormatter.FormateCoordinatePoint("bottom",radial_location.bottom_left))
        url_params.append(UrlFormatter.FormateCoordinatePoint("right",radial_location.top_right))
        url_params.append(UrlFormatter.FormateCoordinatePoint("left",radial_location.bottom_left))
        url_params.append(UrlFormatter.FormateCenterPoint(radial_location.center))
        url_params.append("searchMode=radius")
        url_params.append("radius=%f" % (radial_location.radius*1000))
        url_params.append( "_=%d" %(timestamp))

        url = UrlFormatter._apts_url + quote("&".join(url_params), safe="/=&")
        return url


