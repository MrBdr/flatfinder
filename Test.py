__author__ = 'Admin'
import sys
import Location
import yad2
from os import path
import pickle

import ConfigParser

import telegram
from time import sleep


def getTestChatId(bot_token, bot_name, bot_username):
    import urllib2
    import urllib
    import json

    url = "https://api.telegram.org/bot" + bot_token + "/getUpdates"
    result = urllib2.urlopen(url).read()

    json_result = json.loads(result)
    chat_id = json_result["result"][0][u"message"][u"chat"][u"id"]
    print chat_id
    return chat_id


APTS_DISK_PATH = "viewed_apts.pickle"
apts = []

def saveAptsFromDisk(apts):
    with open(APTS_DISK_PATH, "wb") as pickle_file:
        pickle.dump(apts, pickle_file)


def getAptsFromDisk():
    data = []
    if path.exists(APTS_DISK_PATH):
        with open(APTS_DISK_PATH, "rb") as pickle_file:
            data = pickle.load(pickle_file)

    return data


TEST = False  # for using a test chat


def main(apts):
    if not len(sys.argv) > 1:
        print "bad usage: needs places.ini file"
        exit(0)

    config = ConfigParser.RawConfigParser()
    config.read(sys.argv[1])

    locations = []
    for ini_location in config.sections():
        lat = config.getfloat(ini_location, 'lat')
        lng = config.getfloat(ini_location, 'lng')
        radius = config.getfloat(ini_location, 'radius_in_km')
        name = config.get(ini_location, 'name')

        center = Location.Point(lat, lng)
        locations.append(Location.RadialLocation(center, radius, name))

    poller_sleep_in_seconds = 1800

    bot_token = "155503704:AAHNvdmPH8YuvGZ2B3lB_B7fGHpNXe8FLFQ"
    bot_username = "FlatFinderBot"
    bot_name = "FlatFinder"

    if TEST:
        chat_id = 37671457  # chat with me only

    else:
        chat_id = -28745769  # yad2 group

    bot = telegram.Bot(bot_token, bot_name, bot_username)
    yad2_chat = telegram.Chat(chat_id)
    yad2_site = yad2.Website()

    apts_filter = {
        "fromRooms" : "2.5",
        "untilRooms" : "4",
        "fromPrice" : "3000",
        "untilPrice" : "7000"
    }

    while (True):

        print "querying yad2.."
        for loc in locations:

            yad2_obj = yad2_site.getListings(loc, apts_filter)
            print ""
            print "yad2 response for: '%s'" % loc.name
            if not yad2_obj[u"hasResults"]:
                print "dont have any apts for location"
                sleep(poller_sleep_in_seconds)
                continue

            print "total apts got: %d" % (yad2_obj[u"groups"][0][u"TotalRecords"])

            normal_apts = yad2_obj[u"groups"][0][u"mador"][0]
            realtor_apts = yad2_obj[u"groups"][0][u"mador"][1]
            print "normal apts: %d" % normal_apts[u"total"]
            print "realtor apts: %d" % realtor_apts[u"total"]

            had_new_apts = False

            all_apts = normal_apts[u"results"] + realtor_apts[u"results"]

            for apt_json in all_apts:
                apt_id = apt_json[u"NadlanID"]
                if apt_id in apts:
                    continue

                is_realtor = apt_json in realtor_apts[u"results"]

                print "new apt!"
                sleep(30) # theres a limit to requests per second or something

                had_new_apts = True

                apts.append(apt_id)
                apt_loc = Location.Point(apt_json[u"lat"], apt_json[u"lng"])
                print "sending new apt %s" % apt_id

                # place link
                new_apt_msg = telegram.Message(yad2_site.getApartmentInfoFullUrl(apt_id, is_realtor).encode("utf8"))
                new_apt_msg.remote_id = bot.sendMessage(yad2_chat, new_apt_msg)

                # place address
                addr = "%s %s\n%s" % (apt_json[u"Street"], apt_json[u"HomeNum"], apt_json[u"Neighborhood"])
                bot.sendMessage(yad2_chat, telegram.Message(addr.encode("utf8")))

                # price
                bot.sendMessage(yad2_chat, telegram.Message(apt_json[u"Price"].encode("utf8")))

                # map of place
                bot.sendLocation(yad2_chat, apt_loc.lat, apt_loc.lng)

                # separator
                bot.sendMessage(yad2_chat, telegram.Message("__________________________________"))

            if had_new_apts:
                saveAptsFromDisk(apts)

        print "sleeping until next time..."
        print "____________________________________"
        sleep(poller_sleep_in_seconds)


if __name__ == '__main__':
    try:
        apts = getAptsFromDisk()  # TODO: save even if keyboard interrupted?
        main(apts)
    except Exception as e:
        print "EXCEPTION!"
        print e
        saveAptsFromDisk(apts)
