__author__ = 'Admin'
import httplib
import json

from UrlFormatter import UrlFormatter


class Website:
    _headers = {
        "Cache-Control": "no-cache",
        "DNT": "1",
        "User-Agent:": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate",
        "Connection": "keep-alive",
        "Pragma": "no-cache"
    }

    def __init__(self):
        pass

    def getListings(self, loc, filter = None):

        url = UrlFormatter.GetApartmentsURL(loc, filter)

        conn = httplib.HTTPConnection( UrlFormatter._domain)
        conn.request("GET", url, headers=Website._headers)
        response = conn.getresponse()

        yad2_response = response.read()
        return json.loads(yad2_response)


    def getApartmentInfoUrl(self, apt_id, is_realtor = False):

        url = UrlFormatter.GetInfoURL(apt_id, is_realtor)
        return url


    def getApartmentInfoFullUrl(self, apt_id, is_realtor = False):
        url = UrlFormatter._protocol +\
              UrlFormatter._domain+\
              UrlFormatter.GetInfoURL(apt_id, is_realtor)
        return url



