__author__ = 'Admin'


from GeoLocation import GeoLocation
from Point import Point

class RadialLocation:

    def __init__(self, center, radius, name=""):

        self.radius = radius
        self.center = center
        self.name = name

        sw, ne = RadialLocation.CalculateBoundingBox(center, radius)
        self.bottom_left = Point(sw.deg_lat, sw.deg_lon)
        self.top_right = Point(ne.deg_lat, ne.deg_lon)


    @staticmethod
    def CalculateBoundingBox(center, radius):
        loc = GeoLocation.from_degrees(center.lat, center.lng)
        return loc.bounding_locations(radius)


